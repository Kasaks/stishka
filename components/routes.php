<?php
return array (
'create' => 'main/create',
'test-list' => 'main/list',
'log-in/verification' => 'autch/verification',
'log-in' => 'autch/log_in',
'registration' => 'autch/registration',
'([a-z]+)' => 'error/404',
'' => "main/index"
);