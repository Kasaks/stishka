CREATE TABLE IF NOT EXISTS `users` (
	`user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`user_name` VARCHAR(255) NOT NULL,
	`user_login` VARCHAR(255) NOT NULL,
	`user_password` VARCHAR(255) NOT NULL,
	`user_email` VARCHAR(255) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `tests` (
	`test_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`test_title` VARCHAR(255) NOT NULL,
	`test_comment` VARCHAR(255) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `images` (
	`image_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`image_name` VARCHAR(255) NOT NULL,
	`image_extansion` VARCHAR(255) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `questions` (
	`question_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`question` VARCHAR(255) NOT NULL,
	`question_hint` VARCHAR(255) NOT NULL,
	`question_image_id` INT UNSIGNED,
	FOREIGN KEY (`question_image_id`) REFERENCES `images` (`image_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `answers` (
	`answer_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`answer` VARCHAR(255) NOT NULL,
	`answer_check` BOOLEAN -- поле-указатель верного ответа
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
	
CREATE TABLE IF NOT EXISTS `com_tests__questions` (
	`tests_questions_test_id` INT UNSIGNED NOT NULL,
	`tests_questions_question_id` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`tests_questions_test_id`, `tests_questions_question_id`),
	INDEX `tests_questions_test_id` (`tests_questions_test_id`),
	INDEX `tests_questions_question_id` (`tests_questions_question_id`),
	CONSTRAINT `fk_tests_questions_test_id` FOREIGN KEY (`tests_questions_test_id`) REFERENCES `tests` (`test_id`) ON DELETE CASCADE,
	CONSTRAINT `fk_tests_questions_question_id` FOREIGN KEY (`tests_questions_question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `com_answers__questions` (
	`answers_questions_question_id` INT UNSIGNED NOT NULL,
	`answers_questions_answer_id` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`answers_questions_question_id`, `answers_questions_answer_id`),
	INDEX `answers_questions_question_id` (`answers_questions_question_id`),
	INDEX `answers_questions_answer_id` (`answers_questions_answer_id`),
	CONSTRAINT `fk_answers_on_questions_questions_question_id` FOREIGN KEY (`answers_questions_question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE,
	CONSTRAINT `fk_answers_on_questions_questions_answer_id` FOREIGN KEY (`answers_questions_answer_id`) REFERENCES `answers` (`answer_id`) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `com_users__tests` (
	`users_tests_user_id` INT UNSIGNED NOT NULL,
	`users_tests_test_id` INT UNSIGNED NOT NULL,
	`user_tests_test_check` BOOLEAN, -- поле указывает принадлежит тест пользователю или он был присоеденен к нему
	PRIMARY KEY (`users_tests_user_id`, `users_tests_test_id`),
	INDEX `users_tests_user_id` (`users_tests_user_id`),
	INDEX `users_tests_test_id` (`users_tests_test_id`),
	CONSTRAINT `fk_users_tests_user_id` FOREIGN KEY (`users_tests_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE,
	CONSTRAINT `fk_users_tests_test_id` FOREIGN KEY (`users_tests_test_id`) REFERENCES `tests` (`test_id`) ON DELETE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;