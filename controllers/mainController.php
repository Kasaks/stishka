<?php

include_once './models/queryModel.php';

Class MainController {

	public function action_index () { // выстраивает главную страницу и подтягивает информацию на нее

		include_once './templates/header.php';
		include_once './templates/index.php';
		include_once './templates/footer.php';

		return true;
	}

	public function action_create () { // вытраивает страницу создания теста


		include_once './templates/header.php';
		include_once './templates/create-test.php';
		include_once './templates/footer.php';

		return true;
	}	

	public function action_list ($user_id) { // user_id приходит из сессии

		$result = queryModel::get_test_list();

		include_once './templates/header.php';
		include_once './templates/test-list.php';
		include_once './templates/footer.php';

		return true;
	}

}