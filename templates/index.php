<section class="user-action">
	<div class="row vert-center hor-center wrapper-btn">
		<div>
			<div class="create-test-btn btn">
				<a href="/create">Создать тест</a>
			</div>
			<div class="show-test-list-btn btn">
				<a href="/test-list">Просмотреть список тестов</a>
			</div>
		</div>
	</div>
</section>